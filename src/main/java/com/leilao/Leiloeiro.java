package com.leilao;

import java.util.Collections;
import java.util.List;

public class Leiloeiro {
    private String nome ;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance maiorLance(){

       return (Lance) this.getLeilao().getLances().get(this.getLeilao().getLances().size()-1);




    }

}
