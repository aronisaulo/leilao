package com.leilao;

import com.sun.org.apache.xpath.internal.objects.XObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LeilaoTests {
    @Test
    public void testAdicionarNovoLance(){

        Lance mockLance1 = new Lance(new Usuario("Emerson",10), 10.90);
        Lance mockLance2 = new Lance(new Usuario("Ayrton",1), 11.01);
        Lance mockLance3 = new Lance(new Usuario("Prost",2), 10.20);
        Lance mockLance4 = new Lance(new Usuario("Lafitti",24), 11.0);
        Lance mockLance5 = new Lance(new Usuario("Saulo",3), 18.20);
        Leilao leilao = new Leilao();
        leilao.adicionarNovoLance(mockLance1);
        leilao.adicionarNovoLance(mockLance2);
        leilao.adicionarNovoLance(mockLance3);
        leilao.adicionarNovoLance(mockLance4);
        leilao.adicionarNovoLance(mockLance5);

        Assertions.assertTrue(leilao.getLances().contains(mockLance5));
    }

    @Test
    public void testMaiorLance(){
        Lance mockLance1 = new Lance(new Usuario("Emerson",10), 10.90);
        Lance mockLance2 = new Lance(new Usuario("Ayrton",1), 22.01);
        Lance mockLance3 = new Lance(new Usuario("Saulo",3), 18.20);

        Leilao leilao = new Leilao();

        leilao.adicionarNovoLance(mockLance1);
        leilao.adicionarNovoLance(mockLance2);
        leilao.adicionarNovoLance(mockLance3);

        Leiloeiro leiloeiro = new Leiloeiro("LEILOEIROS TESTE",leilao);
        Lance lanceMaior = leiloeiro.maiorLance();
        Assertions.assertEquals(lanceMaior.getValorDoLance(),mockLance3.getValorDoLance());


    }
}
