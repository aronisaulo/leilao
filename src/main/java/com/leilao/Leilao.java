package com.leilao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Leilao {

    private List<Lance> lances = new ArrayList<>();

    public Leilao() {
    }

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance){
        if (this.validarLance(lance)) {
            this.lances.add(lance);
        }
        return lance;
    }
    public boolean validarLance(Lance lance){

        if (this.getLances().isEmpty())
            return true;
        int ultimoValor = this.getLances().size()-1;
        if (lance.getValorDoLance() > this.getLances().get(ultimoValor).getValorDoLance())
            return true;


       return false;
    }
}
